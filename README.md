# Личный проект «WTW»

---
# О проекте
«Что посмотреть» — онлайн кинотеатр нового поколения. Смотрите новинки абсолютно бесплатно и в лучшем качестве. Оставляйте отзывы, ставьте оценки и выбирайте только лучшее из мира большого кино.

_Не удаляйте и не изменяйте папки и файлы:_
_`.editorconfig`, `.gitattributes`, `.gitignore`._

# commits format must be with structure[init/feat/fix/ref]: [verb] [functionality], 
- feat: implement caruselorfeat: add section 'popular' to blog.
---