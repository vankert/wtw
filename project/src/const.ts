export enum AppRoute {
  Login = "/login",
  Root = "/",
  MyList = "/mylist",
  Film = "/films/:id",
  AddReviews = "/films/:id/review",
  Player = "/player/:id",
}

export enum AuthorizationStatus {
  Auth = "AUTH",
  NoAuth = "NO_AUTH",
  Unknown = "UNKNOWN",
}

export enum APIRoute {
  Films = "/films",
  PromoFilm = "/promo",
  Login = "/login",
  Logout = "/logout",
}

export const TIMEOUT_SHOW_ERROR = 3000;

export const DEFAULT_GENRE: string = "All genres";

export const COUNT_FILMS_STEP: number = 8;
