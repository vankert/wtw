import { createReducer } from "@reduxjs/toolkit";
import { Film } from "../types/film";

import {
  changeGenre,
  countFilms,
  resetCountFilms,
  loadFilms,
  setFilmsByGenre,
  requireAuthorization,
  setError,
  setFilmsDataLoadingStatus,
  loadPromoFilm,
  loadSameFilms,
} from "./action";

import { DEFAULT_GENRE, COUNT_FILMS_STEP, AuthorizationStatus } from "../const";

type InitalState = {
  genre: string;
  films: Film[];
  filmsByGenre: Film[];
  sameFilms: Film[];
  promoFilm: Film;
  filmsCount: number;
  authorizationStatus: AuthorizationStatus;
  error: string | null;
  isFilmsDataLoading: boolean;
};

const initialState: InitalState = {
  genre: DEFAULT_GENRE,
  films: [],
  filmsByGenre: [],
  sameFilms: [],
  promoFilm: {} as Film,
  filmsCount: COUNT_FILMS_STEP,
  authorizationStatus: AuthorizationStatus.Unknown,
  error: null,
  isFilmsDataLoading: false,
};

export const reducer = createReducer(initialState, (builder) => {
  builder
    .addCase(changeGenre, (state, action) => {
      state.genre = action.payload;
    })
    .addCase(countFilms, (state) => {
      state.filmsCount += 8;
    })
    .addCase(resetCountFilms, (state) => {
      state.filmsCount = COUNT_FILMS_STEP;
    })
    .addCase(loadFilms, (state, action) => {
      state.films = action.payload;
    })
    .addCase(loadSameFilms, (state, action) => {
      state.sameFilms = action.payload;
    })
    .addCase(setFilmsByGenre, (state, action) => {
      state.filmsByGenre = action.payload;
    })
    .addCase(loadPromoFilm, (state, action) => {
      state.promoFilm = action.payload;
    })
    .addCase(requireAuthorization, (state, action) => {
      state.authorizationStatus = action.payload;
    })
    .addCase(setError, (state, action) => {
      state.error = action.payload;
    })
    .addCase(setFilmsDataLoadingStatus, (state, action) => {
      state.isFilmsDataLoading = action.payload;
    });
});
