import { createAction } from "@reduxjs/toolkit";
import { Film } from "../types/film";
import { AuthorizationStatus } from "../const";

export const changeGenre = createAction<string>("film/changeGenre");

export const countFilms = createAction("film/countFilms");

export const resetCountFilms = createAction("film/resetCountFilms");

export const loadFilms = createAction<Film[]>("data/loadFilms");

export const loadPromoFilm = createAction<Film>("data/loadPromoFilm");

export const loadSameFilms = createAction<Film[]>("data/loadSameFilms");

export const setFilmsByGenre = createAction<Film[]>("film/setFilmsByGenre");

export const requireAuthorization = createAction<AuthorizationStatus>(
  "user/requireAuthorization"
);

export const setError = createAction<string | null>("game/setError");

export const setFilmsDataLoadingStatus = createAction<boolean>(
  "data/setFilmsDataLoadingStatus"
);
