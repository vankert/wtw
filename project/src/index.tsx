import React from "react";
import ReactDOM from "react-dom/client";
import App from "./components/app/app";
import { Provider } from "react-redux";

import ErrorMessage from "./components/ErrorMessage/ErrorMessage";

import { store } from "./store";

import {
  fetchFilmAction,
  checkAuthAction,
  fetchPromoFilmAction,
} from "./store/api-actions";

store.dispatch(fetchFilmAction());
store.dispatch(fetchPromoFilmAction());
store.dispatch(checkAuthAction());

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);

root.render(
  <React.StrictMode>
    <Provider store={store}>
      <ErrorMessage />
      <App />
    </Provider>
  </React.StrictMode>
);
