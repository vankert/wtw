import { Helmet } from "react-helmet-async";

import GenresList from "../../components/GenresList/GenresList";
import FilmCardList from "../../components/FilmsList/FilmsList";
import FilmCard from "../../components/FilmCard/FilmCard";
import Footer from "../../components/Footer/Footer";
import ShowMoreButton from "../../components/ShowMoreButton/ShowMoreButton";

import { useAppSelector } from "../../hooks";
import { AuthorizationStatus } from "../../const";
import LoadingScreen from "../../pages/LoadingScreen/LoadingScreen";

function Main(): JSX.Element {
  const films = useAppSelector((state) => state.films);
  const promoFilm = useAppSelector((state) => state.promoFilm);

  const authorizationStatus = useAppSelector(
    (state) => state.authorizationStatus
  );
  const isFilmsDataLoading = useAppSelector(
    (state) => state.isFilmsDataLoading
  );

  if (
    authorizationStatus === AuthorizationStatus.Unknown ||
    isFilmsDataLoading
  ) {
    return <LoadingScreen />;
  }

  return (
    <>
      <Helmet>
        <title>WTW Выбери что посмотреть!</title>
      </Helmet>

      <FilmCard promoFilm={promoFilm} />

      <div className="page-content">
        <section className="catalog">
          <h2 className="catalog__title visually-hidden">Catalog</h2>

          <GenresList films={films} />

          <FilmCardList />

          <ShowMoreButton />
        </section>

        <Footer />
      </div>
    </>
  );
}

export default Main;
