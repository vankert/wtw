import { Helmet } from "react-helmet-async";
import { Link } from "react-router-dom";
import { useEffect } from "react";

import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";
import NotFound from "../NotFound/NotFound";
import FilmTabs from "../../components/FilmTabs/FilmTabs";
import FilmCardList from "../../components/FilmsList/FilmsList";

import { useAppSelector } from "../../hooks";
import { useParams } from "react-router-dom";

import { store } from "../../store";
import { fetchSameFilmAction } from "../../store/api-actions";

import { AuthorizationStatus } from "../../const";

function MoviePage(): JSX.Element {
  const films = useAppSelector((state) => state.films);
  const params = useParams();
  const film = films.find((film) => film.id === Number(params.id));

  const loginStatus = useAppSelector((state) => state.authorizationStatus);

  useEffect(() => {
    store.dispatch(fetchSameFilmAction(Number(params.id)));
  }, []);

  return film ? (
    <>
      <Helmet>
        <title>WTW Фильм.</title>
      </Helmet>
      <section className="film-card film-card--full">
        <div className="film-card__hero">
          <div className="film-card__bg">
            <img src={film?.backgroundImage} alt={film?.name} />
          </div>

          <h1 className="visually-hidden">WTW</h1>

          <Header />

          <div className="film-card__wrap">
            <div className="film-card__desc">
              <h2 className="film-card__title">{film?.name}</h2>
              <p className="film-card__meta">
                <span className="film-card__genre">{film?.genre}</span>
                <span className="film-card__year">{film?.released}</span>
              </p>

              <div className="film-card__buttons">
                <button
                  className="btn btn--play film-card__button"
                  type="button"
                >
                  <svg viewBox="0 0 19 19" width="19" height="19">
                    <use xlinkHref="#play-s"></use>
                  </svg>
                  <span>Play</span>
                </button>
                {loginStatus === AuthorizationStatus.Auth ? (
                  <>
                    <button
                      className="btn btn--list film-card__button"
                      type="button"
                    >
                      <svg viewBox="0 0 19 20" width="19" height="20">
                        <use xlinkHref="#add"></use>
                      </svg>
                      <span>My list</span>
                      <span className="film-card__count">9</span>
                    </button>
                    <Link
                      to={`/films/${film.id}/review`}
                      className="btn film-card__button"
                    >
                      Add review
                    </Link>
                  </>
                ) : null}
              </div>
            </div>
          </div>
        </div>

        <div className="film-card__wrap film-card__translate-top">
          <div className="film-card__info">
            <div className="film-card__poster film-card__poster--big">
              <img
                src={film?.posterImage}
                alt={film?.name}
                width="218"
                height="327"
              />
            </div>

            <div className="film-card__desc">
              <FilmTabs film={film} />
            </div>
          </div>
        </div>
      </section>

      <div className="page-content">
        <section className="catalog catalog--like-this">
          <h2 className="catalog__title">More like this</h2>

          <FilmCardList sameFilms />
        </section>

        <Footer />
      </div>
    </>
  ) : (
    <NotFound />
  );
}

export default MoviePage;
