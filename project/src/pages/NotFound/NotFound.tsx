import {Helmet} from 'react-helmet-async';

import Logo from "../../components/Logo/Logo";

function NotFound(): JSX.Element {
    return (
        <>
        <Helmet>
            <title>WTW 404!</title>
        </Helmet>
            <Logo></Logo>
            <h1>404 Page not found</h1>
        </>
    )
}

export default NotFound;
