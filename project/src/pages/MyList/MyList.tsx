import {Helmet} from 'react-helmet-async';

import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";

function MyList(): JSX.Element {
    return (
      <div className="user-page">
        <Helmet>
          <title>WTW Список моих фильмов.</title>
        </Helmet>
        <Header/>

        <section className="catalog">
          <h2 className="catalog__title visually-hidden">Catalog</h2>

          <div className="catalog__films-list">

          </div>
        </section>

        <Footer/>
    </div>
    );
}

export default MyList;
