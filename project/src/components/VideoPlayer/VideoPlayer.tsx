import { useEffect, useRef } from 'react';

import { Film } from "../../types/film";

type VideoPlayerComponentProps = {
    film: Film;
}

function VideoPlayer({film}: VideoPlayerComponentProps): JSX.Element {
    const videoRef = useRef<HTMLVideoElement | null>(null);

  useEffect(() => {
    const startPlayTimeout = setTimeout(
      () => videoRef.current?.play(),
      1000
    );

    return () => clearTimeout(startPlayTimeout);
  });

    return (
        <video
      src={film.previewVideoLink}
      className="player__video"
      poster={film.posterImage}
      ref={videoRef}
      muted
      loop
    />
    );
}

export default VideoPlayer;
