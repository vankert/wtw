import { Link } from "react-router-dom";

import { Film } from "../../types/film";
import { useAppSelector, useAppDispatch } from "../../hooks";
import {
  changeGenre,
  resetCountFilms,
  setFilmsByGenre,
} from "../../store/action";

type GenresListProps = {
  films: Film[];
};

function GenresList({ films }: GenresListProps): JSX.Element {
  const uniqueGenres = [...new Set(films.map((film) => film.genre))];
  const activeGenre = useAppSelector((state) => state.genre);

  const dispatch = useAppDispatch();

  const onFilterChange = (genre: string) => {
    dispatch(changeGenre(genre));
    dispatch(resetCountFilms());

    const filmsByGenre = films.filter((film) => film.genre === genre);
    dispatch(setFilmsByGenre(filmsByGenre));
  };

  return (
    <ul className="catalog__genres-list">
      <li
        className={`catalog__genres-item ${
          "All genres" === activeGenre && "catalog__genres-item--active"
        }`}
      >
        <Link
          to={"#"}
          className="catalog__genres-link"
          onClick={() => onFilterChange("All genres")}
        >
          All genres
        </Link>
      </li>
      {uniqueGenres.map((genre) => {
        return (
          <li
            key={genre}
            className={`catalog__genres-item ${
              genre === activeGenre && "catalog__genres-item--active"
            }`}
          >
            <Link
              to={"#"}
              className="catalog__genres-link"
              onClick={() => onFilterChange(genre)}
            >
              {genre}
            </Link>
          </li>
        );
      })}
    </ul>
  );
}

export default GenresList;
