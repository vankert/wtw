import { Fragment } from 'react';
import { Film } from "../../types/film";

type DetailsTabComponentProps = {
    film: Film;
  }

function DetailsTab({film}: DetailsTabComponentProps) {

    const transformIntToHour = (minutes: number) => {
        const hoursItem = Math.floor(minutes / 60);
        const minutesItem = Math.floor(minutes % 60);

        const hoursDisplay = hoursItem > 0 ? `${hoursItem} h ` : '';
        const minutesDisplay = minutesItem > 0 ? `${minutesItem} m ` : '';
        return hoursDisplay + minutesDisplay;

      };

    return (
            <div className="film-card__text film-card__row">
              <div className="film-card__text-col">
                <p className="film-card__details-item">
                  <strong className="film-card__details-name">Director</strong>
                  <span className="film-card__details-value">{film.director}</span>
                </p>
                <p className="film-card__details-item">
                  <strong className="film-card__details-name">Starring</strong>
                  <span className="film-card__details-value">
                  {film.starring.map((item) => <Fragment key={item}>{item} <br/></Fragment>)}
                  </span>
                </p>
              </div>

              <div className="film-card__text-col">
                <p className="film-card__details-item">
                  <strong className="film-card__details-name">Run Time</strong>
                  <span className="film-card__details-value">{transformIntToHour(film.runTime)}</span>
                </p>
                <p className="film-card__details-item">
                  <strong className="film-card__details-name">Genre</strong>
                  <span className="film-card__details-value">{film.genre}</span>
                </p>
                <p className="film-card__details-item">
                  <strong className="film-card__details-name">Released</strong>
                  <span className="film-card__details-value">{film.released}</span>
                </p>
              </div>
            </div>
    )
}

export default DetailsTab;
