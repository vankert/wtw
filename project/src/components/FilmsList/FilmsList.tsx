import SmallFilmCard from "../SmallFilmCard/SmallFilmCard";
import { useAppSelector } from "../../hooks";
import { DEFAULT_GENRE } from "../../const";

type FilmCardListComponentProps = {
  sameFilms?: boolean;
};

function FilmCardList({ sameFilms }: FilmCardListComponentProps): JSX.Element {
  const currentFilmsCountStep = useAppSelector((state) => state.filmsCount);

  const sameFilmsList = useAppSelector((state) => state.sameFilms).slice(0, 4);

  const currentFilmsList = useAppSelector((state) => state.films);
  const currentFilmsByGenre = useAppSelector((state) => state.filmsByGenre);
  const currentGenre = useAppSelector((state) => state.genre);

  const currentShownFilmsList = currentFilmsList.slice(
    0,
    currentFilmsCountStep
  );

  const currentShownFilmsByGenre = currentFilmsByGenre.slice(
    0,
    currentFilmsCountStep
  );

  return (
    <div className="catalog__films-list">
      {sameFilms
        ? sameFilmsList.map((film) => {
            return <SmallFilmCard key={film.id} film={film} />;
          })
        : currentGenre != DEFAULT_GENRE
        ? currentShownFilmsByGenre.map((film) => {
            return <SmallFilmCard key={film.id} film={film} />;
          })
        : currentShownFilmsList.map((film) => {
            return <SmallFilmCard key={film.id} film={film} />;
          })}
    </div>
  );
}

export default FilmCardList;
