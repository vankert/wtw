import { Route, BrowserRouter, Routes } from "react-router-dom";
import { HelmetProvider } from "react-helmet-async";
import { useAppSelector } from "../../hooks";
import { AppRoute, AuthorizationStatus } from "../../const";

import Main from "../../pages/Main/Main";
import SignIn from "../../pages/SignIn/SignIn";
import MyList from "../../pages/MyList/MyList";
import MoviePage from "../../pages/MoviePage/MoviePage";
import AddReview from "../../pages/AddReview/AddReview";
import Player from "../../pages/Player/Player";
import NotFound from "../../pages/NotFound/NotFound";
import ScrollToTop from "../ScrollToTop/ScrollToTop";

import PrivateRoute from "../PrivateRoute/PrivateRoute";

function App(): JSX.Element {
  const authorizationStatus = useAppSelector(
    (state) => state.authorizationStatus
  );

  return (
    <HelmetProvider>
      <BrowserRouter>
        <ScrollToTop />
        <Routes>
          <Route path={AppRoute.Root} element={<Main />} />
          <Route path={AppRoute.Login} element={<SignIn />} />
          <Route path={AppRoute.Film} element={<MoviePage />} />
          <Route
            path={AppRoute.MyList}
            element={
              <PrivateRoute authorizationStatus={authorizationStatus}>
                <MyList />
              </PrivateRoute>
            }
          />
          <Route
            path={AppRoute.AddReviews}
            element={
              <PrivateRoute authorizationStatus={authorizationStatus}>
                <AddReview />
              </PrivateRoute>
            }
          />
          <Route path={AppRoute.Player} element={<Player />} />
          <Route path="*" element={<NotFound />} />
        </Routes>
      </BrowserRouter>
    </HelmetProvider>
  );
}

export default App;
