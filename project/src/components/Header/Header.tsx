import { Link } from "react-router-dom";

import Logo from "../Logo/Logo";

import { useAppSelector, useAppDispatch } from "../../hooks";
import { MouseEvent } from "react";
import { AuthorizationStatus, AppRoute } from "../../const";

import { logoutAction } from "../../store/api-actions";

function Header(): JSX.Element {
  const loginStatus = useAppSelector((state) => state.authorizationStatus);
  const dispatch = useAppDispatch();

  const onLogOut = (evt: MouseEvent<HTMLAnchorElement>) => {
    evt.preventDefault();
    dispatch(logoutAction());
  };

  return (
    <header className="page-header user-page__head">
      <Logo />

      {loginStatus === AuthorizationStatus.Auth ? (
        <>
          <h1 className="page-title user-page__title">
            My list <span className="user-page__film-count">9</span>
          </h1>
          <ul className="user-block">
            <li className="user-block__item">
              <div className="user-block__avatar">
                <img
                  src="img/avatar.jpg"
                  alt="User avatar"
                  width="63"
                  height="63"
                />
              </div>
            </li>
            <li className="user-block__item">
              <a onClick={onLogOut} className="user-block__link">
                Sign out
              </a>
            </li>
          </ul>
        </>
      ) : (
        <ul className="user-block">
          <li className="user-block__item">
            <Link to={AppRoute.Login} className="user-block__link">
              Sign in
            </Link>
          </li>
        </ul>
      )}
    </header>
  );
}

export default Header;
