import { useState } from "react";
import cn from "classnames"

import OverviewTab from "../OverviewTab/OverviewTab";
import DetailsTab from "../DetailsTab/DetailsTab";
import ReviewsTab from "../ReviewsTab/ReviewsTab";

import { Film } from "../../types/film";

type FilmTabsComponentProps = {
    film: Film;
  }

function FilmTabs({film}: FilmTabsComponentProps) {
    const [activeTab, setActiveTab] = useState('Overview');

    const handleClick = (evt: React.MouseEvent<HTMLAnchorElement, MouseEvent> & {target: {tagName: string; textContent: string}}) => {
        if (evt.target.tagName === 'A') {
          evt.preventDefault();
          setActiveTab(evt.target.textContent);
        }
      };

    return (
        <>
        <nav className="film-nav film-card__nav">
            <ul className="film-nav__list">
                <li className={activeTab === 'Overview' ? 'film-nav__item film-nav__item--active' : 'film-nav__item'}>
                    <a onClick={handleClick} href="#" className="film-nav__link">Overview</a>
                </li>
                <li className={activeTab === 'Details' ? 'film-nav__item film-nav__item--active' : 'film-nav__item'}>
                    <a onClick={handleClick} href="#" className="film-nav__link">Details</a>
                </li>
                <li className={activeTab === 'Reviews' ? 'film-nav__item film-nav__item--active' : 'film-nav__item'}>
                    <a onClick={handleClick} href="#" className="film-nav__link">Reviews</a>
                </li>
            </ul>
        </nav>

        {
            (activeTab === 'Overview') ? <OverviewTab film={film}/>
            : (activeTab === 'Details') ? <DetailsTab film={film}/>
            : (activeTab === 'Reviews') ? <ReviewsTab/>
            : <OverviewTab film={film}/>
        }
        </>
    )
}

export default FilmTabs;
