import { Film } from "../../types/film";
import {Link} from 'react-router-dom';
import { useState} from "react";
import VideoPlayer from "../VideoPlayer/VideoPlayer";

type SmallFilmCardComponentProps = {
    film: Film;
}

function SmallFilmCard({film}: SmallFilmCardComponentProps): JSX.Element {
  const [cardHovered, setCardHovered] = useState(false);

  return (
        <article
          onMouseEnter={()=>setCardHovered(true)}
          onMouseLeave={()=>setCardHovered(false)}
          className="small-film-card catalog__films-card"
        >
            <div className="small-film-card__image">
              {cardHovered ? <VideoPlayer film = {film}/> : <img src={film.previewImage} alt={film.name} width="280" height="175" />}

            </div>
            <h3 className="small-film-card__title">
              <Link className="small-film-card__link" to={`/films/${film.id}`}>{film.name}</Link>
            </h3>
        </article>
    );
}

export default SmallFilmCard;
