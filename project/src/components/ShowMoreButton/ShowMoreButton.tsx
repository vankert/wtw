import { useAppSelector, useAppDispatch } from "../../hooks";
import { countFilms } from "../../store/action";
import { DEFAULT_GENRE } from "../../const";

function ShowMoreButton() {
  const films = useAppSelector((state) => state.films);
  const filmsByGenre = useAppSelector((state) => state.filmsByGenre);
  const currentGenre = useAppSelector((state) => state.genre);
  const currentCount = useAppSelector((state) => state.filmsCount);

  const numberOfFilms =
    currentGenre != DEFAULT_GENRE ? filmsByGenre.length : films.length;

  const dispatch = useAppDispatch();

  const onFilmsCountChange = () => {
    dispatch(countFilms());
  };

  return numberOfFilms <= currentCount ? null : (
    <div className="catalog__more">
      <button
        onClick={onFilmsCountChange}
        className="catalog__button"
        type="button"
      >
        Show more
      </button>
    </div>
  );
}

export default ShowMoreButton;
